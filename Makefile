SRC_DIR := src
CONCATENATED_NOTEBOOK := main.ipynb
PUBLIC_DIR := public

env:
	python -m venv venv
	venv/bin/pip install -r requirements.txt

$(CONCATENATED_NOTEBOOK): $(SRC_DIR)/*.ipynb
	nbmerge $(SRC_DIR)/*.ipynb -o $(CONCATENATED_NOTEBOOK)

slides: $(CONCATENATED_NOTEBOOK)
	jupyter nbconvert $(CONCATENATED_NOTEBOOK) --to slides --output-dir=$(PUBLIC_DIR)
	mv $(PUBLIC_DIR)/main.slides.html $(PUBLIC_DIR)/index.html
	mv main.ipynb $(PUBLIC_DIR)/main.ipynb
	cp -r media $(PUBLIC_DIR)/media

clean:
	rm -f $(CONCATENATED_NOTEBOOK)
	rm -rf $(PUBLIC_DIR)

.PHONY: env clean slides
