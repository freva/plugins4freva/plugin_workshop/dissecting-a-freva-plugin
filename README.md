# Dissecting a Freva Plugin

[![Jupyter Notebook](https://img.shields.io/badge/Public-Jupyter%20Notebook-blue)](https://freva.gitlab-pages.dkrz.de/plugins4freva/plugin_workshop/dissecting-a-freva-plugin/main.ipynb)
[![Slides](https://img.shields.io/badge/Slides-HTML-orange)](https://freva.gitlab-pages.dkrz.de/plugins4freva/plugin_workshop/dissecting-a-freva-plugin)


This repository serves as the slideshow for the `Dissecting a Freva Plugin` section of the [Freva workshop held on June 13th](https://events.dkrz.de/event/65/#17-dissecting-a-freva-plugin). It covers the following topics:

- What is a Freva Wrapper?
- How To Install All Needed Libraries Of A Plugin
- How To Plug a Tool into Freva
- Connecting your IDE / Code Editor to a remote session using SSH

## Usage on Local Machine

1. **Setup Environment**:
    ```sh
    make env
    ```

2. **Activate Environment**:
    ```sh
    source venv/bin/activate
    ```

3. **Generate Slides**:
    ```sh
    make slides
    ```

4. **Clean Build Files**:
    ```sh
    make clean
    ```

### To avoid typo in your contents, use `pre-commit` setup

To use the pre-commit setup, follow these steps:

1. **Activate Hooks**:
   - run the following command to set up the git hooks:
     ```bash
     pre-commit install
     ```

2. **Automatic Checks**:
   - Now, each time you attempt to commit changes, the pre-commit hooks will automatically run to ensure your files adhere to the specified standards.

3. **Manual Execution**:
   - If you want to manually run the hooks on all files, use:
     ```bash
     pre-commit run --all-files
     ```

To view the generated slides, open the `public/index.html` file in your browser. For access to the Jupyter notebook of the slides, click `public/main.ipynb` .




